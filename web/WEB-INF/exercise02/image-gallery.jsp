<%--
  Created by IntelliJ IDEA.
  User: tcro142
  Date: 25/09/2018
  Time: 8:21 PM
  To change this template use File | Settings | File Templates.
--%>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <title>Photomographical Thingos</title>
</head>
<body>
<div id="images" class="container">
    <c:forEach var="image" items="${images}">
        <section class="image">
                <img src="./Photos/${image.getName()}">
            <p>${image.getName()}</p>
        </section>

    </c:forEach>
</div>
</body>
</html>
