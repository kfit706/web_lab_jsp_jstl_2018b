package ictgradschool.web.simplewebapp.exercise01;
//this is the biography servlet
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Biography extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("HI!");

        String firstName = req.getParameter("firstname");
        String lastName = req.getParameter("lastname");
        String biographyContent = req.getParameter("biography");

        req.setAttribute("firstname",firstName);
        req.setAttribute("lastname",lastName);
        req.setAttribute("biography",biographyContent);


        //Get the parameters from your form and create session attributes to store them

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/exercise01/biography.jsp");
        dispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
