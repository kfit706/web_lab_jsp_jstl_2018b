package ictgradschool.web.simplewebapp.Gallery;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Gallery extends HttpServlet {

    private File photoDirectory;

    //DONE setup the photo directory as an instance variable

    public void init() throws ServletException {
        //TODO get the photo directory and store it in the instance variable you created
        //super.init(); DONT NEED THIS
        this.photoDirectory = new File(getServletContext().getRealPath("/Photos"));
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Inside Gallery Servlet doGet method...");

        //TODO use the getFileDataList method to get all images
        List<File> images = getFileDataList(photoDirectory);

        //TODO create a session attribute to store all images
        req.getSession().setAttribute("images",images);

        //TODO call the displayGallery method to display the image gallery
        displayGallery(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    private List<File> getFileDataList(File folder) {
        //TODO create a file list to store all image files
        List<File> photos = new ArrayList<>();//had = null but needed to be = newarraylist
        File[] images = folder.listFiles();
        //TODO use a loop to loop through all image files and add them to the list

        for (File image : images) {
            photos.add(image);
        }
            //TODO return the list of images
            return photos;
        }

        private void displayGallery (HttpServletRequest req, HttpServletResponse resp) throws
        ServletException, IOException {

            // TODO use dispatcher.forward to forward to 'image-gallery.jsp'
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/exercise02/image-gallery.jsp");
            dispatcher.forward(req, resp);

        }

    }

